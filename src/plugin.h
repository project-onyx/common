// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <QtCore/QObject>

/**
 * @file        Plugin.h
 * @brief       Contient la déclaration de la classe Plugin
 * @details     La classe \c Plugin permet de montrer l'utilisation des \em tags \b Doxygen
 * @author      Matthieu Jacquemet <Matthieu.jacquemet@univ-lyon1.fr>
 * @date        2021
 */
class Plugin {

public:
    Plugin() = default;
    virtual ~Plugin() = default;

    virtual void initialize() = 0;
};

Q_DECLARE_INTERFACE(Plugin, "org.onyx.Plugin")

#endif // __PLUGIN_H__