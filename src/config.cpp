// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QRegularExpression>

#include "abstractVariableProvider.h"
#include "config.h"


void Config::registerProvider(AbstractVariableProvider* provider) {
    m_providers.insert(provider->name(), provider);
}

// returns all the keys that match the pattern ex: "foo.*" will return all the
// keys that start with "foo." and "foo?" will return all the keys named "foo"
// followed by any character.
QStringList Config::keys(const QString& pattern) {

    QStringList keys = allKeys();
    QStringList result;
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QRegularExpression regexp(QRegularExpression::wildcardToRegularExpression(pattern));
#else
    QRegularExpression regexp = QRegularExpression::fromWildcard(pattern);
#endif
    for (const QString& key : keys) {
        QRegularExpressionMatch match = regexp.match(key);
        if (match.hasMatch()) {
            result.append(key);
        }
    }
    return result;
}

// this function substitutes every occurrence of ${provider:key} with the
// value of the provider key.
QString Config::substituteVariables(const QString& value) const {
    QString result = value;
    QRegularExpression rx("\\$\\{([^:]+):([^}]+)\\}");
    int pos = 0;

    while (pos < result.length()) {
        QRegularExpressionMatch match = rx.match(result, pos);
        if (match.hasMatch()) {
            QString name = match.captured(1);
            QString key = match.captured(2);
            AbstractVariableProvider* provider = m_providers.value(name);

            if (provider != nullptr) {
                result.replace(match.capturedStart(), match.capturedLength(),
                                provider->variable(key));

                pos = match.capturedStart() + value.length();
            } else {
                pos = match.capturedEnd();
            }
        } else {
            pos = result.length();
        }
    }
    return result;
}


QVariant Config::value(const QString& key, const QVariant& defaultValue) const {

    QVariant variant = QSettings::value(key);

    if (variant.isNull()) {
        variant = defaultValue;
    }
    if (variant.type() == QVariant::String) {
        return substituteVariables(variant.toString());
    }
    return variant;
}
