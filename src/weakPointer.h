// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __WEAKPOINTER_H__
#define __WEAKPOINTER_H__

#include <QSharedPointer>


template<class Type>
class WeakPointer: public QWeakPointer<Type> {

public:
    WeakPointer() = default;
    WeakPointer(const QSharedPointer<Type>& other);
    WeakPointer(const QWeakPointer<Type>& other);

    WeakPointer<Type>& operator=(const QObject *other);
    WeakPointer<Type>& operator=(const QWeakPointer<Type>& other);

    constexpr Type& operator *() const noexcept;
    constexpr Type* operator -> () const noexcept;
    constexpr operator Type *() const noexcept;
};


template<class Type>
WeakPointer<Type>::WeakPointer(const QSharedPointer<Type>& other):
    QWeakPointer<Type>(other)
{
    
}

template<class Type>
WeakPointer<Type>::WeakPointer(const QWeakPointer<Type>& other):
    QWeakPointer<Type>(other)
{
    
}

template<class Type>
WeakPointer<Type>& WeakPointer<Type>::operator=(const QObject* other) {
    QWeakPointer<Type>::operator =(other);
    return *this;
}

template<class Type>
WeakPointer<Type>& WeakPointer<Type>::operator=(const QWeakPointer<Type>& other) {
    QWeakPointer<Type>::operator=(other);
    return *this;
}

template<class Type>
constexpr Type* WeakPointer<Type>::operator ->() const noexcept {
    return QWeakPointer<Type>::toStrongRef().data();
}

template<class Type>
constexpr Type& WeakPointer<Type>::operator *() const noexcept {
    *QWeakPointer<Type>::toStrongRef().data();
}

template<class Type>
constexpr WeakPointer<Type>::operator Type *() const noexcept {
    return QWeakPointer<Type>::toStrongRef().data();
}

#endif // __WEAKPOINTER_H__