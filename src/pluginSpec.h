// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLUGINSPEC_H__
#define __PLUGINSPEC_H__

#include <QSharedPointer>
#include <QPluginLoader>


class Plugin;
class Dependency;


class PluginSpec: public QObject {

    Q_OBJECT

public:
    typedef QList<PluginSpec*> Plugins;
    enum State { Invalid, Read, Resolved, Loaded, Error };

    PluginSpec(const QString& filename);
    ~PluginSpec() = default;

    bool readLibrary(const QString& iid);
    bool loadLibrary();
    bool initialize();
    bool resolveDependencies(const Plugins& plugins);
    bool reportError(const QString& reason);

    Plugins resolved() const;
    Plugin* instance() const;
    State state() const;

    QString name, version, vendor, copyright;
    QString license, description, url, category;

    int compareVersion(const QString& other) const;
    bool satisfies(const QString& name, const QString& version) const;

private:
    bool readMetaData(const QJsonObject& data, const QString& iid);
    bool loadDependencies(Plugins& loading);

    struct Dependency {
        QString name, version;
    };
    typedef QList<Dependency> Dependencies;
    Dependencies m_dependencies;
    Plugins m_resolved;

    QPluginLoader m_loader;
    QSharedPointer<Plugin> m_instance;

    State m_state;
};

#endif // __PLUGINSPEC_H__