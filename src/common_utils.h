// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __COMMON_UTILS_H__
#define __COMMON_UTILS_H__

#include <QMetaEnum>

#include "bidirectionalMap.h"

QMetaMethod findMethod(QObject* object, const QRegularExpression& reg);


template<typename EnumType>
static inline BidirectionalMap<QString, EnumType> readEnum() {
    BidirectionalMap<QString, EnumType> map;

    QMetaEnum metaEnum = QMetaEnum::fromType<EnumType>();
    static_assert(std::is_enum<EnumType>::value);

    for (int i = 0; i < metaEnum.keyCount(); ++i) {
        map.insert(metaEnum.key(i), EnumType(metaEnum.value(i)));
    }
    return map;
}

template<class T, class V>
static inline bool tryInsert(T& container, const V& value) {
    static_assert(std::is_same<typename T::value_type, V>::value);

    int count = container.count();
    container.insert(value);
    return count != container.count();
}

template<class T>
T mapValue(T value, T inMin, T inMax, T outMin, T outMax) {
    return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

#endif // __COMMON_UTILS_H__