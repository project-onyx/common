// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SVGICONENGINE_H__
#define __SVGICONENGINE_H__

#include <QIconEngine>
#include <QHash>

class QSvgRenderer;


class SvgIconEngine : public QIconEngine {

public:
    SvgIconEngine(QSvgRenderer* renderer, const QString& name);
    virtual ~SvgIconEngine() = default;

    QPixmap pixmap( const QSize& size, QIcon::Mode mode,
                    QIcon::State state) override;
    
    void paint(QPainter *painter, const QRect &rect, QIcon::Mode mode,
                QIcon::State state) override;

    QIconEngine *clone() const override;
    QString key() const override;
    QString iconName() override;
    QList<QSize> availableSizes(QIcon::Mode mode = QIcon::Normal,
                                QIcon::State state = QIcon::Off) 
                                override;
private:
    QSvgRenderer* const m_renderer;
    const QString m_name;

    QHash<QPair<QSize, QIcon::State>, QPixmap> m_pixmapCache;
};

#endif // __SVGICONENGINE_H__