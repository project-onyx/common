// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CIRCULARBUFFER_H__
#define __CIRCULARBUFFER_H__

#include <array>

#include "common_pot_utils.h"


template<class DataType, POTValues potsize>
class CircularBuffer {

public:
    inline CircularBuffer() : m_head(0), m_tail(0) {}
    enum {
        BufferSize = pot_to_int(potsize),
        BufferMask = BufferSize - 1
    };
    void push(DataType value) noexcept {
        m_buffer[m_head++] = value;
        m_head &= BufferMask;
    }
    DataType pop() noexcept {
        DataType value = m_buffer[m_tail++];
        m_tail &= BufferMask;
        return value;
    }
    size_t size() const noexcept {
        return (size_t(m_head) - size_t(m_tail)) & BufferMask;
    }
private:
    typedef std::array<DataType, BufferSize> Buffer;
    Buffer m_buffer;
    quint16 m_head, m_tail;
};

#endif // __CIRCULARBUFFER_H__