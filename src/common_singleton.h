// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __COMMON_SINGLETON_H__
#define __COMMON_SINGLETON_H__

#include <QtGlobal>

/**
 * @brief provided an interface for accessing single instance of T
 * @note This class is intended to be used as CRTP
 */
template<class ClassType, class Parent=QObject>
class Singleton: public Parent {

public:
    static ClassType* instance() {
        static ClassType _instance;
        return &_instance;
    }
protected:
    Singleton() = default;
    Q_DISABLE_COPY_MOVE(Singleton)
};

#endif // __COMMON_SINGLETON_H__
