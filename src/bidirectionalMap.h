// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __BIDIRECTIONALMAP_H__
#define __BIDIRECTIONALMAP_H__

#include <QHash>


template<class KeyType, class ValueType>
class BidirectionalMap: public QHash<KeyType, ValueType> {

public:
    typedef QHash<ValueType, KeyType> ReverseMap;
    typedef QHash<KeyType, ValueType> Base;

    using Base::contains;
    using ThisType = BidirectionalMap<KeyType, ValueType>;

    BidirectionalMap() = default;
    BidirectionalMap(const ThisType& other) = default;

    BidirectionalMap(std::initializer_list<std::pair<KeyType,ValueType>> list)
        : Base(list)
    {
        m_reverseMap.reserve(list.size());

        for (auto it = this->begin(); it != this->end(); ++it) {
            m_reverseMap.insert(it.value(), it.key());
        }
    }

    inline ThisType& operator=(const ThisType& other) noexcept {

        Base::operator=(other);
        m_reverseMap = other.m_reverseMap;
        return *this;
    }

    inline ThisType& operator=(ThisType&& other) noexcept {

        Base::operator=(std::move(other));
        m_reverseMap = std::move(other.m_reverseMap);
        return *this;
    }

    inline void remove(const KeyType& key) noexcept {
        ValueType value = Base::value(key);
        Base::remove(key);
        m_reverseMap.remove(value);
    }

    inline typename Base::iterator insert(const KeyType& key, const ValueType& value) {
        m_reverseMap.insert(value, key);
        return Base::insert(key, value);
    }

    inline KeyType key(const ValueType& value, KeyType def=KeyType()) const noexcept {
        return m_reverseMap.value(value);
    }

    inline bool contains(const ValueType& value) const noexcept {
        return m_reverseMap.contains(value);
    }

    inline ReverseMap reverseMap() const noexcept {
        return m_reverseMap;
    }

    inline void clear() noexcept {
        Base::clear();
        m_reverseMap.clear();
    }

private:
    ReverseMap m_reverseMap;
};


#endif // __BIDIRECTIONALMAP_H__