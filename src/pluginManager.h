// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PLUGINMANAGER_H__
#define __PLUGINMANAGER_H__

#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtCore/QVector>

#include "pluginSpec.h"
#include "plugin.h"


class PluginManager final: public QObject {
    
    Q_OBJECT

public:
    PluginManager(const QString& iid, QObject* parent = nullptr);
    ~PluginManager() = default;

    void readPlugins(const QDir& path);
    void initializePlugins();

    typedef QList<Plugin*> Plugins;

    Plugins pluginsByCategory(const QString& id) const;
    Plugin* pluginByName(const QString& name) const;

    QString iid() const noexcept;
    template<class T> T* findPlugin() const;

    bool isPluginLoaded(const QString& name, const QString& version) const;

private:
    Q_DISABLE_COPY_MOVE(PluginManager)
    PluginManager() = default;

    typedef QVector<PluginSpec*> SpecsQueue;

    void resolveDependencies();

    const QString m_iid;

    typedef QList<QSharedPointer<PluginSpec>> PluginSpecs;
    PluginSpecs m_plugins;
};


template<class T>
T* PluginManager::findPlugin() const {
    
    Q_STATIC_ASSERT_X(QtPrivate::HasQ_OBJECT_Macro<T>::Value,
        "qobject_cast requires the type to have a Q_OBJECT macro");

    for (QSharedPointer<PluginSpec>& spec: m_plugins) {
        if (spec->state() != PluginSpec::Loaded) {
            continue;
        } else if (T* found = qobject_cast<T*>(spec->instance())) {
            return found;
        }
    }
}


#endif // __PLUGINMANAGER_H__