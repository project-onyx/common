// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __COMMON_POT_UTILS_H__
#define __COMMON_POT_UTILS_H__

#include <qglobal.h>


enum POTValues {
    POT_invalid = -1, POT_1 = 0, POT_2, POT_4, POT_8, POT_16,
    POT_32, POT_64, POT_128, POT_256, POT_512, POT_1024, POT_2048,
    POT_4096, POT_8192, POT_16384, POT_32768, POT_65536
};

inline constexpr quint16 pot_to_int(POTValues pot) {
    return 0x1u << pot;
}

inline POTValues int_to_pot(quint16 value) {
    if (Q_UNLIKELY(value == 0)) {
        return POT_invalid;
    }
    for (int i = 0; i < POT_65536; i++) {
        if (value == (0x1u << i)) {
            return static_cast<POTValues>(i);
        }
    }
    return POT_invalid;
}

#endif // __COMMON_POT_UTILS_H__