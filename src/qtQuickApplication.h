// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __QTQUICKAPPLICATION_H__
#define __QTQUICKAPPLICATION_H__

#include <QQuickView>
#include <QGuiApplication>

class QQuickView;
class QQmlApplicationEngine;
class QFileSystemWatcher;
class QTimer;

class QtQuickApplication : public QGuiApplication {

    Q_OBJECT
    Q_PROPERTY(QQuickView* mainView READ mainView CONSTANT)

public:
    QtQuickApplication(int &argc, char **argv);

    QQuickView* mainView() const noexcept;

signals:
    void viewSourceReloading();

protected:
    void setMainViewUrl(const QUrl& mainViewUrl);

    QTimer* m_timer;
    QQmlApplicationEngine* m_engine;
    QQuickView* m_mainView;
#ifndef NDEBUG
    QFileSystemWatcher* m_filesystemWatcher;
#endif
};


#endif // __QTQUICKAPPLICATION_H__