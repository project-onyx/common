// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <tuple>


template <size_t n, typename... T>
inline typename std::enable_if<(n >= sizeof...(T))>::type
    serialize_tuple(QDBusArgument&, const std::tuple<T...>&)
{}

template <size_t n, typename... T>
inline typename std::enable_if<(n >= sizeof...(T))>::type
    deserialize_tuple(const QDBusArgument&, std::tuple<T...>&)
{}

template <size_t n, typename... T>
inline typename std::enable_if<(n < sizeof...(T))>::type
    serialize_tuple(QDBusArgument& stream, const std::tuple<T...>& tup)
{
    stream << std::get<n>(tup);
    serialize_tuple<n+1>(stream, tup);
}

template <size_t n, typename... T>
inline typename std::enable_if<(n < sizeof...(T))>::type
    deserialize_tuple(const QDBusArgument& stream, std::tuple<T...>& tup)
{
    stream >> std::get<n>(tup);
    deserialize_tuple<n+1>(stream, tup);
}

template <typename... T>
inline QDBusArgument& operator<<(QDBusArgument& stream, const std::tuple<T...>& tup) {
    stream.beginStructure();
    serialize_tuple<0>(stream, tup);
    stream.endStructure();

    return stream;
}

template <typename... T>
inline const QDBusArgument& operator>>(const QDBusArgument& stream, std::tuple<T...>& tup) {
    stream.beginStructure();
    deserialize_tuple<0>(stream, tup);
    stream.endStructure();

    return stream;
}