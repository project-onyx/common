// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __ABSTRACTVARIABLEPROVIDER_H__
#define __ABSTRACTVARIABLEPROVIDER_H__


#include <QObject>


class AbstractVariableProvider: public QObject {

    Q_OBJECT

public:
    AbstractVariableProvider(const QString& name, QObject* parent = nullptr);
    virtual ~AbstractVariableProvider() = default;

    virtual QString variable(const QString& key) const = 0;

    QString name() const noexcept;

private:
    const QString& m_name;
};


#endif // __ABSTRACTVARIABLEPROVIDER_H__