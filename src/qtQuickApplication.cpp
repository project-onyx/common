// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QFileSystemWatcher>
#include <QDirIterator>
#include <QFileInfo>
#include <QTimer>

#include "qtQuickApplication.h"

// #include "macro_utils.h"

// INITIALIZER(register_common_types) {
//     qRegisterMetaType<QQuickView*>("QQuickView*");
// }



QStringList listFiles(const QString& dir, const QStringList& filters) {

    QStringList files;
    QDirIterator it(dir, filters, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        files << it.next();
    }
    return files;
}


QtQuickApplication::QtQuickApplication(int &argc, char **argv):
    QGuiApplication(argc, argv),
    m_timer(new QTimer(this)),
    m_engine(new QQmlApplicationEngine(this)),
    m_mainView(new QQuickView(m_engine, nullptr))
#ifndef NDEBUG
    , m_filesystemWatcher(new QFileSystemWatcher(this)) 
#endif
{
    m_timer->setInterval(100);
    m_timer->setSingleShot(true);

    m_mainView->setResizeMode(QQuickView::SizeRootObjectToView);

    connect(m_timer, &QTimer::timeout, [this] {
        emit viewSourceReloading();
        m_engine->clearComponentCache();
        m_mainView->setSource(m_mainView->source());
    });
    // Reload all qml files when they are modified
#ifndef NDEBUG
    connect(m_filesystemWatcher, &QFileSystemWatcher::fileChanged,
            m_timer, QOverload<>::of(&QTimer::start));

    connect(m_filesystemWatcher, &QFileSystemWatcher::directoryChanged,
        [&](const QString& dirPath) {
            m_filesystemWatcher->addPath(dirPath);
        }
    );
#endif // NDEBUG
}


QQuickView* QtQuickApplication::mainView() const noexcept {
    return m_mainView;
}


void QtQuickApplication::setMainViewUrl(const QUrl& mainViewUrl) {

    m_mainView->setSource(mainViewUrl);

#ifndef NDEBUG
    // clear all previous connections to the file system watcher
    if (!m_filesystemWatcher->files().empty()) {
        m_filesystemWatcher->removePaths(m_filesystemWatcher->files());
    }
    if (!m_filesystemWatcher->directories().empty()) {
        m_filesystemWatcher->removePaths(m_filesystemWatcher->directories());
    }

    QFileInfo mainQmlInfo(mainViewUrl.path());
    QString mainQmlDir = mainQmlInfo.absolutePath();

    m_filesystemWatcher->addPaths(listFiles(mainQmlDir,
        {"*.qml", "*.glsl", "*.frag", "*.vert", "*.comp"}
    ));
#endif
}

    