// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __COMMON_MACRO_UTILS_H__
#define __COMMON_MACRO_UTILS_H__


#define COMMA ,
#define CAT(A, B) A ## B
#define MACRO_CONCAT(a, b) CAT(a,b)
#define SELECT(NAME, NUM) CAT(NAME ## _, NUM)
#define COMPOSE(NAME, ARGS) NAME ARGS

#define GET_COUNT(_0, _1, _2, _3, _4, _5, _6, COUNT, ...) COUNT
#define EXPAND() ,,,,,,

#define VA_SIZE(...) COMPOSE(GET_COUNT, (EXPAND __VA_ARGS__ (),0,6,5,4,3,2,1))
#define FOR_EACH(M, ...) SELECT(ARGS, VA_SIZE(__VA_ARGS__))(M, __VA_ARGS__)
#define VA_SELECT(M, ...) SELECT(M, VA_SIZE(__VA_ARGS__))(__VA_ARGS__)

#define ARGS_0(M, X)      
#define ARGS_1(M, X)      M(X)
#define ARGS_2(M, X, ...) M(X)ARGS_1(M, __VA_ARGS__)
#define ARGS_3(M, X, ...) M(X)ARGS_2(M, __VA_ARGS__)
#define ARGS_4(M, X, ...) M(X)ARGS_3(M, __VA_ARGS__)
#define ARGS_5(M, X, ...) M(X)ARGS_4(M, __VA_ARGS__)
#define ARGS_6(M, X, ...) M(X)ARGS_5(M, __VA_ARGS__)


#if defined(__GNUC__ ) || defined(__clang__)
    #define LIKELY(X) __builtin_expect(!!(X), 1)
    #define UNLIKELY(X) __builtin_expect(!!(X), 0)

    #define NOEXPORT __attribute__((visibility("hidden")))
    #define EXPORT __attribute__((visibility("default")))
#else
    #define LIKELY(X) (X)
    #define UNLIKELY(X) (X)

    #define NOEXPORT
    #define EXPORT
#endif


#define TYPE_OF(VALUE) std::remove_reference<decltype(VALUE)>::type

#define METHOD_FN(OBJ, FUNC) std::bind(std::mem_fn(&TYPE_OF(*OBJ)::FUNC), \
                                    OBJ, std::placeholders::_1)


#define RESOLVE(CLASS, FUNC, ...) QOverload<__VA_ARGS__>::of(&CLASS::FUNC)


#define METHOD_TYPE(NAME) decltype(std::declval<T>().NAME(), void())


#define CHECK_HAS_METHOD(NAME) \
    template<class T, typename = void> \
    struct has_ ## NAME: std::false_type {}; \
    template<class T> \
    struct has_ ## NAME<T, METHOD_TYPE(NAME)>: std::true_type {};


#define HAS_METHOD(CLS, METHOD) has_##METHOD<CLS>::value


#define DEFINE_LITERAL(FROM, TO) \
    inline constexpr TO operator "" _##TO(FROM value) noexcept { \
        return static_cast<TO>(value); \
    }


#if defined(_MSC_VER)
    #pragma section(".CRT$XCU",read)
    #define INITIALIZER2_IMPL(ID,p) \
        static void ID(void); \
        __declspec(allocate(".CRT$XCU")) void (*ID##_)(void) = ID; \
        __pragma(comment(linker,"/include:" p #ID "_")) \
        static void ID(void)
    #ifdef _WIN64
        #define INITIALIZER_IMPL(ID) INITIALIZER2_IMPL(f,"")
    #else
        #define INITIALIZER_IMPL(ID) INITIALIZER2_IMPL(f,"_")
    #endif
#elif defined(__GNUC__) || defined(__clang__)
    #define INITIALIZER_IMPL(ID) \
        static void ID(void) __attribute__((constructor)); \
        static void ID(void)
#elif defined(__cplusplus)
    #define INITIALIZER_IMPL(ID) \
        static void init_f_ ## ID(void); \
        struct init_t_ ## ID { init_t_ ## ID(void) { init_f_ ## ID(); } }; \
        static init_t_ ## ID init_i_ ## ID; \
        static void init_f_ ## ID(void)
#endif

#define INITIALIZER_ID(ID) INITIALIZER_IMPL(ID)
#define INITIALIZER(ID) INITIALIZER_ID(MACRO_CONCAT(ID, __COUNTER__))


#define C_STR(STR) (STR).toUtf8().data()

#define GETTER(CLS, NAME) \
    inline auto CLS::NAME() const -> decltype(m_ ## NAME) { \
        return m_ ## NAME; \
    }





#endif // __COMMON_MACRO_UTILS_H__