// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QCoreApplication>
#include <QSvgRenderer>
#include <QPainter>
#include <qpair.h>

#include "svgIconengine.h"

// quint64 qHash(const QSize& size) {
//     return (quint64(size.width()) << 32) | quint64(size.height());
// }

SvgIconEngine::SvgIconEngine(QSvgRenderer* renderer, const QString& name):
    m_renderer(renderer), m_name(name) {
    
}

QIconEngine* SvgIconEngine::clone() const {
    return new SvgIconEngine(*this);
}

QString SvgIconEngine::key() const {
    return QLatin1String("SvgIconEngine");
}

QString SvgIconEngine::iconName() {
    return m_name;
}

QList<QSize> SvgIconEngine::availableSizes(QIcon::Mode mode, QIcon::State state) {

    static QList<QSize> sizes;
    if (Q_UNLIKELY(sizes.isEmpty())) {
        for (int i = 10; i <= 32; ++i) sizes.append(QSize(i, i));
    }
    return sizes;
}

QPixmap SvgIconEngine::pixmap(  const QSize& size, QIcon::Mode mode,
                                QIcon::State state) {
    
    QPixmap& pixmap = m_pixmapCache[qMakePair(size, state)];

    if (Q_UNLIKELY(pixmap.isNull())) {
        pixmap = QPixmap(size);
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);

        QString name = m_name;
        switch (state) {
            case QIcon::On: name    += QStringLiteral("_on"); break; 
            case QIcon::Off: name   += QStringLiteral("_off"); break;
        };
        if (!m_renderer->elementExists(name)) {
            name = m_name;
        }
        QRectF rect = m_renderer->boundsOnElement(name);
        rect.moveTo(0.0, 0.0);
        if (rect.height() > rect.width()) {
            rect.setSize(rect.size() * (qreal(size.width()) / rect.width()));
            rect.moveTop((qreal(size.height()) - rect.height()) * 0.5);
        }
        else if (rect.width() > rect.height()) {
            rect.setSize(rect.size() * (qreal(size.height()) / rect.height()));
            rect.moveLeft((qreal(size.width()) - rect.width()) * 0.5);
        }
        m_renderer->render(&painter, name, std::move(rect));
    }
    return pixmap;
}


void SvgIconEngine::paint( QPainter *painter, const QRect &rect, QIcon::Mode mode,
                            QIcon::State state) {
    
    const qreal dpr = !qApp->testAttribute(Qt::AA_UseHighDpiPixmaps) ?
            qreal(1.0) : painter->device()->devicePixelRatioF();

    QSize pixmapSize = rect.size() * dpr;
    painter->drawPixmap(rect, pixmap(rect.size(), mode, state));
}


