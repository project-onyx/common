// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QJsonArray>
#include <QMap>
#include <QMetaEnum>

#include "plugin.h"
#include "pluginSpec.h"


const QRegularExpression versionReg("([0-9]+)(?:[.]([0-9]+))?(?:[.]([0-9]+))?(?:_([0-9]+))?");


template<class T>
static inline bool readValue(T& result, const QJsonValue& value) {

    result = value.toVariant().value<T>();
    return !value.isUndefined();
}


PluginSpec::PluginSpec(const QString& filename):
    m_loader(filename),
    m_state(State::Invalid) {
    
}



bool PluginSpec::readLibrary(const QString& iid) {

    if (m_state != State::Invalid) {
        return reportError("Plugin already loaded");
    }
    else if (m_loader.fileName().isEmpty()) {
        qWarning("Cannot open plugin");
    }
    else if (readMetaData(m_loader.metaData(), iid)) {
        return m_state = Read;
    }
    return false;
}


bool PluginSpec::satisfies(const QString& name, const QString& version) const {
    
    if (name.compare(this->name, Qt::CaseInsensitive) != 0) {
        return false;
    }
    return compareVersion(version) >= 0;
}


bool PluginSpec::resolveDependencies(const Plugins& plugins) {

    if (m_state != State::Read) {
        return reportError("Cannot resolve dependencies");
    }
    Dependencies::iterator it = m_dependencies.begin();

    while (it != m_dependencies.end()) {
        auto found = std::find_if(plugins.begin(), plugins.end(), 
        [&](PluginSpec* spec) {
            return spec->satisfies(it->name, it->version);
        });
        if (Q_LIKELY(found != plugins.end())) {
            m_resolved.append(*found), m_dependencies.erase(it++);
        } else {
            return false;
        }
    }
    return m_state = State::Resolved;
}


bool PluginSpec::loadLibrary() {

    if (m_state != State::Resolved) return false;

    if (Q_UNLIKELY(!m_loader.load())) {
        return reportError(m_loader.errorString());
    }
    QObject* plugin = m_loader.instance();

    if (Plugin* instance = qobject_cast<Plugin*>(plugin)) {
        m_instance.reset(instance);
        return m_state = State::Loaded;
    }
    plugin->deleteLater();
    return reportError("Not derived from Plugin");;
}


bool PluginSpec::initialize() {
    
    if (m_state != State::Loaded) {
        return reportError("Plugin not loaded");
    }
    Q_ASSERT(!m_instance.isNull());

    m_instance->initialize();
    return true;
}


bool PluginSpec::readMetaData(const QJsonObject& data, const QString& iid) {

    QJsonValue value;
    QString pluginIID;
    QJsonObject metadata = data["MetaData"].toObject();

    if (!readValue(pluginIID, data.value("IID"))) {
        return reportError("IID is not defined");
    } else if (pluginIID != iid) {
        return reportError("IID does not match " + iid);
    }
    
    if (!readValue(name, metadata.value("Name"))) {
        readValue(name, data.value("className"));
    } else if (name.isEmpty()) {
        return reportError("Name is empty");
    }
    if (!readValue(version, metadata.value("Version"))) {
        version = "1.0";
    } else if (!versionReg.match(version).hasMatch()) {
        return reportError("Version is invalid");
    }

    readValue(vendor, metadata.value("Vendor"));
    readValue(copyright, metadata.value("Copyright"));
    readValue(license, metadata.value("License"));
    readValue(description, metadata.value("Description"));
    readValue(url, metadata.value("Url"));
    readValue(category, metadata.value("Category"));

    QJsonArray array = metadata["Dependencies"].toArray();
    QJsonArray array2 = metadata["LoadHints"].toArray();

    for (const QJsonValue val: array) {
        const QJsonObject depData = val.toObject();
        QString depName, depVersion;
        
        if (!readValue(depName, depData.value("Name"))) {
            return reportError("Dependency name is undefined");
        } else if (depName.isEmpty()) {
            return reportError("Dependency name is empty");
        }
        if (readValue(depVersion, depData.value("Version")) &&
            !versionReg.match(depVersion).hasMatch())
        {
            return reportError("Dependency version is undefined");
        }
        m_dependencies.append(Dependency {
            .name = depName,
            .version = depVersion
        });
    }

    QMetaEnum hintsMeta = QMetaEnum::fromType<QLibrary::LoadHints>();
    QLibrary::LoadHints hints = m_loader.loadHints();

    for (const QJsonValue val: array2) {
        QString hintName = val.toString();
        bool ok;
        int hint = hintsMeta.keyToValue(hintName.toLatin1(), &ok);
        if (Q_LIKELY(ok)) {
            hints |= static_cast<QLibrary::LoadHint>(hint);
        } else {
            qWarning("Unknown load hint %", qPrintable(hintName));
        }
    }
    m_loader.setLoadHints(hints);

    return true;
}


bool PluginSpec::reportError(const QString& reason) {
    
    qWarning("Error for plugin %s : %s",    qPrintable(m_loader.fileName()),
                                            qPrintable(reason));
    m_state = Error;
    return false;
}


Plugin* PluginSpec::instance() const {
    return m_instance.get();
}

PluginSpec::State PluginSpec::state() const {
    return m_state;
}

PluginSpec::Plugins PluginSpec::resolved() const {
    return m_resolved;
}


int PluginSpec::compareVersion(const QString& other) const {

    QRegularExpressionMatch match0 = versionReg.match(version);
    QRegularExpressionMatch match1 = versionReg.match(other);

    if (!match0.hasMatch() || !match1.hasMatch()) {
        return 0;
    }
    int number0, number1;

    for (int i = 0; i < 4; ++i) {
        number0 = match0.captured(i + 1).toInt();
        number1 = match1.captured(i + 1).toInt();
        
        if (number0 != number1) {
            return number0 < number1 ? -1 : 1;
        }
    }
    return 0;
}

















    // if (m_state == Resolving) {
    //     return reportError("Circular dependency");
    // }
    // if (m_state == Loaded) {
    //     return true;
    // }
    // m_state = Resolving;

    // auto findDep = [&](const Dependency& dep) -> PluginSpec* {
    //     for (PluginSpec* spec: plugins) {
    //         if (satisfyDependency(spec))
    //             return spec;
    //     }
    //     return Q_NULLPTR;
    // };
    // for (const Dependency dep: m_dependencies) {
    //     PluginSpec* spec = findDep(dep);

    //     if (spec == Q_NULLPTR) {
    //         m_state = Read;
    //     } else if (spec->resolve(plugins)) {
    //         continue;
    //     }
    //     return false;
    // }
    // return loadLibrary();