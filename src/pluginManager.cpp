// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QPluginLoader>
#include <QJsonArray>

#include "common_utils.h"
#include "plugin.h"
#include "pluginManager.h"
#include "pluginSpec.h"



bool loadPluginSpecQueue(PluginSpec* plugin, QVector<PluginSpec*>& loaded,
                        QSet<PluginSpec*>& cycleCheck) {

    if (plugin->state() == PluginSpec::Loaded) {
        return true;
    } else if (!tryInsert(cycleCheck, plugin)) {
        return plugin->reportError("Circular dependency");
    }
    for (PluginSpec* dep: plugin->resolved()) {
        if (!loadPluginSpecQueue(dep, loaded, cycleCheck)) {
            return false;
        }
    }
    if (plugin->loadLibrary()) {
        loaded.append(plugin); return true;
    }
    return false;
}



PluginManager::PluginManager(const QString& iid, QObject* parent):
    QObject(parent),
    m_iid(iid) {
    
}


void PluginManager::readPlugins(const QDir& path) {

    for (const QString& basename: path.entryList(QDir::Files)) {
        QString fullpath = path.absoluteFilePath(basename);
        
        if (QLibrary::isLibrary(fullpath)) {
            auto spec = QSharedPointer<PluginSpec>::create(fullpath);

            if (spec->readLibrary(m_iid)) {
                m_plugins.append(qMove(spec));
            }
        }
    }
    resolveDependencies();
}


PluginManager::Plugins PluginManager::pluginsByCategory(const QString& id) const {

    Plugins plugins;

    for (QSharedPointer<PluginSpec> spec: m_plugins) {
        if (spec->state() != PluginSpec::Loaded) {
            continue;
        } else if (spec->category == id) {
            plugins.append(spec->instance());
        }
    }
    return plugins;
}


Plugin* PluginManager::pluginByName(const QString& name) const {

    for (QSharedPointer<PluginSpec> spec: m_plugins) {
        if (spec->state() != PluginSpec::Loaded) {
            continue;
        } else if (spec->name == name) {
            return spec->instance();
        }
    }
    return nullptr;
}


QString PluginManager::iid() const noexcept {
    return m_iid;
}


bool PluginManager::isPluginLoaded( const QString& name,
                                    const QString& version) const {
    
    for (QSharedPointer<PluginSpec> spec: m_plugins) {
        if (spec->state() != PluginSpec::Loaded) {
            continue;
        } else if (spec->satisfies(name, version)) {
            return true;
        }
    }
    return false;
}


void PluginManager::initializePlugins() {

    SpecsQueue queue;

    for (QSharedPointer<PluginSpec>& spec: m_plugins) {
        QSet<PluginSpec*> cycleCheck;
        loadPluginSpecQueue(spec.get(), queue, cycleCheck);
    }
    for (PluginSpec* spec: queue) {
        spec->initialize();
    }
}


void PluginManager::resolveDependencies() {

    QList<PluginSpec*> specs;
    specs.reserve(m_plugins.size());

    for (QSharedPointer<PluginSpec> spec: m_plugins) {
        if (spec->state() == PluginSpec::Read) {
            specs.append(spec.get());
        }
    }
    for (PluginSpec* spec: specs) {
        spec->resolveDependencies(specs);
    }
}


